# Treeconstruction

## code: To reconstruct tree shapes from images

Run `reconstruct.m` to perform fit routines on the tree images.
Matlab and various standard toolboxes are required. 

## data: To generate tree shapes and images

Run `./run_script` to generate a set of tree images, and tree metadata.
Blender and Python are required.

## text: Commentary on both processes.

Run `make` to generate all PDFs. `make text` converts the PDF to text.
