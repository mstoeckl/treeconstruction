function mask = detect_foreground(image)


lab = rgb2lab(image);

top_color = [73.67520926, -19.69900661, -40.7782229];
bottom_color = [86.87820448, -41.66834787, -20.50160386];
mid_color = [81.80688327, -33.60661813, -28.2165747];

typical_tree_color = [72.63125438, -4.88289203, 19.2524886];

colors = [top_color; bottom_color; mid_color; typical_tree_color];

[w,h,~] = size(image);

distances = zeros([w,h,4]);
for i = 1:4
    delta = bsxfun(@minus,lab,reshape(colors(i,:),[1,1,3]));
    dsts = sum(delta.^2,3);
    distances(:,:,i) = dsts;
end

bgdist = min(distances(:,:,1:3),[],3);
fgdist = distances(:,:,4);
mask = fgdist < bgdist;
mask = imclose(mask,ones(3));

cc = bwconncomp(mask);
numPixels = cellfun(@numel,cc.PixelIdxList);
[biggest,idx] = max(numPixels);

base = zeros(size(mask),'logical');
base(cc.PixelIdxList{idx}) = true;

mask = base;