function frames = find_camera_frames(vector_data, truth, imsizes)
    nframes = length(vector_data);
    
    [pairs, npairs] = list_pairs(nframes);

    pair_offsets = [];

            
    cI = get_tree_coordinates(truth.tree);
    pI = ones([length(cI),4]);
    for i=1:length(cI)
        pI(i,1:3) = lookup_coord(cI{i}, truth.tree);
    end
    
    for k = 1:npairs
        iA = pairs(k,1);
        iB = pairs(k,2);
    
        cA = get_tree_coordinates(vector_data{iA});
        cB = get_tree_coordinates(vector_data{iB});

        pA = ones([length(cA),3]);
        for i=1:length(cA)
            pA(i,1:2) = lookup_coord(cA{i}, vector_data{iA});
        end
        pB = ones([length(cB),3]);
        for i=1:length(cB)
            pB(i,1:2) = lookup_coord(cB{i}, vector_data{iB});
        end
        
        sigmaA = closest_distances(pA);
        sigmaB = closest_distances(pB);

        cfor = @(x)bcfor(x,truth,imsizes(iA,:),imsizes(iB,:), pA, pB, sigmaA, sigmaB, pI);
        
        nbrute = 50;
        angles = linspace(0, 2 * pi, nbrute);
        scores = [];
        for i=1:nbrute
            scores(i) = cfor(angles(i));
        end
        
        [~,im] = min(scores);
        low = angles(mod(im-1-1,nbrute)+1);
        high = angles(mod(im+1-1,nbrute)+1);
        if low > high
            low = low - 2 * pi;
        end
        angle = fminbnd(cfor,low,high);

        pair_offsets(k) = angle;
        fprintf('Fit: %8.5f vs actual %8.5f\n', angle, mod(truth.camera_frame_angles(iB) - truth.camera_frame_angles(iA), 2 * pi));
    end

    alt_angles = [];
    for i=2:nframes
        for k=1:npairs
            if pairs(k,1) == i && pairs(k,2) == 1
                alt_angles(i-1) =  pair_offsets(k);
            end
        end
    end
    
    fpair_cost = @(a)pair_cost(a, pairs, pair_offsets);
    
    x = fminsearch(fpair_cost, alt_angles);
    aangles = [0, x] + truth.camera_frame_angles(1);
    aangles = mod(aangles, 2 * pi);

    va = sprintf('%8.5f ', aangles);
    vb = sprintf('%8.5f ', truth.camera_frame_angles);
    fprintf('Composite: %s vs actual %s\n', va, vb);
    
    proj_mtxs = {};
    for i=1:nframes
        proj_mtxs{i} = truth.camera_projection_mtx * model_matrix_for_angle(aangles(i), truth);
    end
    
    frames = proj_mtxs;
end


function predicted = model_matrix_for_angle(zangle, truth)
    zrot = [cos(zangle),  sin(zangle), 0, 0; ...
            -sin(zangle), cos(zangle), 0, 0; ...
            0, 0, 1, 0; 0, 0, 0, 1];
    predicted = truth.camera_base_modelview_mtx * zrot;
end


function cost = pair_cost(alt_angles, pairs, pair_offsets)
    aangles = [0, alt_angles];

    cost = 0;
    for k=1:size(pairs,1)
        iA = pairs(k, 1);
        iB = pairs(k, 2);
        off = pair_offsets(k);
        cost = cost + modcircledistance(aangles(iB) - aangles(iA), off).^2;
    end
end


function c = bcfor(a, truth, imA, imB, pA, pB, sigmaA, sigmaB, pI)
    modA = model_matrix_for_angle(0, truth);
    modB = model_matrix_for_angle(a, truth);
    
    pxtA = get_pixel_transform(imA);
    pxtB = get_pixel_transform(imB);
    hpA = pxtA * truth.camera_projection_mtx * modA;
    hpB = pxtB * truth.camera_projection_mtx * modB;

    if nargin == 8
        errs = compute_error_matrix(hpA, hpB, pA, pB, sigmaA, sigmaB);
    else
        errs = compute_error_matrix(hpA, hpB, pA, pB, sigmaA, sigmaB, pI);
    end

    c = sum(min(errs,[],1)) + sum(min(errs, [], 2));
end

function dist = modcircledistance(angleA, angleB)
    angleA = mod(angleA, 2 * pi);
    angleB = mod(angleB, 2 * pi);
    dist = min(abs(angleA - angleB), 2 * pi - abs(angleA - angleB));
end
