function tree3d = find_tree(frames, vector_data, imsizes)
    N = length(vector_data);

    coords = {};
    points = {};
    sigmas = {};
    hproj = {};
    branches = {};
    vno_to_b = {};
    for i = 1:N
        c = get_tree_coordinates(vector_data{i});
        b = containers.Map();
        for k=1:length(c)
            d = c{k};
            b(nodekey(d(1:end-1))) = [-1,-1];
        end
        h = {};
        for k=1:length(c)
            d = c{k};
            
            gg = b(nodekey(d(1:end-1)));
            if d(end) == -1
                gg(1) = k;
            elseif d(end) == -2
                gg(2) = k;
            end
            b(nodekey(d(1:end-1))) = gg;
            h{k} = d(1:end-1);
        end

        p = ones([length(c),3]);
        for j=1:length(c)
            p(j,1:2) = lookup_coord(c{j}, vector_data{i});
        end
        
        branches{i} = b;
        vno_to_b{i} = h;
        points{i} = p;
        coords{i} = c;
        sigmas{i} = closest_distances(p);
        
        hproj{i} = get_pixel_transform(imsizes(i,:)) * frames{i};
    end

    [pairs, npairs] = list_pairs(N);
    nmatchings = containers.Map();
    keymap = containers.Map();
    for k=1:npairs
        i = pairs(k,1);
        j = pairs(k,2);
        
        emtx = compute_error_matrix(hproj{i}, hproj{j}, points{i}, points{j}, sigmas{i}, sigmas{j});
            
        [~, fwd] = min(emtx, [], 2);
        [~, rvs] = min(emtx, [], 1);

        for z = 1:length(fwd)
            kv = {i, vno_to_b{i}{z}, j, vno_to_b{j}{fwd(z)}};
            kk = strcat(ebkey(kv{1},kv{2}), '|', ebkey(kv{3},kv{4}));
            kj = strcat(ebkey(kv{3},kv{4}), '|', ebkey(kv{1},kv{2}));
            keymap(kk) = kv;
            keymap(kj) = kv;
            
            if ~isKey(nmatchings, kk)
                nmatchings(kk) = 0;
            end
            nmatchings(kk) = nmatchings(kk) + 1;
            
            if ~isKey(nmatchings, kj)
                nmatchings(kj) = 0;
            end
            nmatchings(kj) = nmatchings(kj) + 1;
        end
        
        for z = 1:length(rvs)
            kv = {j, vno_to_b{j}{z}, i, vno_to_b{i}{rvs(z)}};
            kk = strcat(ebkey(kv{1},kv{2}), '|', ebkey(kv{3},kv{4}));
            kj = strcat(ebkey(kv{3},kv{4}), '|', ebkey(kv{1},kv{2}));
            keymap(kk) = kv;
            keymap(kj) = kv;
            
            if ~isKey(nmatchings, kk)
                nmatchings(kk) = 0;
            end
            nmatchings(kk) = nmatchings(kk) + 1;
            
            if ~isKey(nmatchings, kj)
                nmatchings(kj) = 0;
            end
            nmatchings(kj) = nmatchings(kj) + 1;
        end
    end
    
    link = containers.Map();
    idmap = containers.Map();
    for k = keys(nmatchings)
        k = k{1};
        
        kv = keymap(k);
        kk = strcat(ebkey(kv{1},kv{2}), '|', ebkey(kv{3},kv{4}));
        if kk ~= k
            kv = {kv{3},kv{4},kv{1},kv{2}};
        end

        if nmatchings(k) >= 3
            pka = ebkey(kv{1},kv{2});
            pkb = ebkey(kv{3},kv{4});
            if ~isKey(link, pka)
                link(pka) = {};
            end
            if ~isKey(link, pkb)
                link(pkb) = {};
            end
            s = link(pka);
            s{end+1} = pkb;
            link(pka) = s;
            s = link(pkb);
            s{end+1} = pka;
            link(pkb) = s;

            idmap(pka) = {kv{1},kv{2}};
            idmap(pkb) = {kv{3},kv{4}};
        end
    end
    
    visited = containers.Map();
    for k = keys(link)
        visited(k{1}) = false;
    end
    
    common_branches = {};
    group_for_branch = containers.Map();
    cl = {};
    for i = 1:N
        kw = ebkey(i, []);
        idmap(kw) = {i,[]};
        visited(kw) = true;
        group_for_branch(kw) = 1;
        cl{end+1} = kw;
    end
    common_branches{end+1} = cl;
    
    for cur = keys(visited)
        cur = cur{1};
        if visited(cur)
            continue
        end
        blist = {cur};
        visited(cur) = true;

        while true
            nv = 0;
            for option = link(cur)
                if visited(option{1})
                    continue
                end
                nv = option{1};
            end
            if nv == 0
                break
            end
            cur = nv;
            blist{end+1} = cur;
            visited(cur) = true;
        end

        if length(blist) >= 2
            common_branches{end+1} = blist;
            for i = 1:length(blist)
                group_for_branch(blist{i}) = length(common_branches);
            end
        end
    end
    
    parent_list = [];
    data_list = {};
    for bgroup = common_branches
        bgroup = bgroup{1};
    
        parents = [];
        for j=1:length(bgroup)
            fbd = idmap(bgroup{j});
            fid = fbd{1};
            bid = fbd{2};
            
            for i = length(bid):-1:1
                key = ebkey(fid, bid(1:end-1));
                if isKey(group_for_branch, key)
                    parents(end+1) = group_for_branch(key);
                end
            end
        end

        if length(parents)
            es = zeros([1,max(parents)]);
            for i=1:length(parents)
                es(parents(i)) = es(parents(i)) + 1;
            end
            [~, pid] = max(es);
        else
            pid = -1;
        end

        vd = {};
        fd = {};
        im = zeros([length(bgroup),2]);
        for j=1:length(bgroup)
            fbd = idmap(bgroup{j});
            fid = fbd{1};
            bid = fbd{2};
            
            vd{j} = get_branch_data(vector_data{fid}, bid);
            fd{j} = frames{fid};
            im(j, :) = imsizes(fid, :);
        end

        data = reconstruct_branch(vd, fd, im, pid < 0);
        
        parent_list(end+1) = pid;
        data_list{end+1} = data;
    end

    tree3d = build_recursive_tree(1, parent_list, data_list);
    fprintf('Reconstructed %d branches\n',length(parent_list));
end

function [X, xi] = reconstruct_point(pts, pprj)
    M = size(pprj,3);
    Z = [];
    for i=1:M
        Z = [Z; crossmtx(pts(i,:)) * squeeze(pprj(:,:,i))];
    end

    [~, s, v] = svd(Z);
    [~, vi] = sort(abs(diag(s)));
    X = v(:, vi(1));
    X = X ./ X(4);

    xi = zeros([M, 2]);
    for i=1:M
        x = squeeze(pprj(:,:,i)) * X;
        xi(i,:) = x(1:2) ./ x(3);
    end
    X = X(1:3)';
end

function ndata = reconstruct_branch(branch_data_list, frame_list, imsizes, isroot)
    M = length(branch_data_list);
    
    fpts = ones([M,3]);
    lpts = ones([M,3]);
    pprj = zeros([3,4,M]);
    lns = [];
    for i = 1:M
        fpts(i,1:2) = branch_data_list{i}(1,:);
        lpts(i,1:2) = branch_data_list{i}(end,:);

        prj = get_pixel_transform(imsizes(i,:)) * frame_list{i};
        pprj(:,:,i) = prj;
        lns(i) = length(branch_data_list{i});
    end

    if isroot
        spt = zeros([1,3]);
    else
        [spt, ~] = reconstruct_point(fpts, pprj);
    end
    [lpt, ~] = reconstruct_point(lpts, pprj);
    
    [~, longest] = max(lns);
    
    maxlen = lns(longest);
    nsplits = int32(log2(maxlen / 10));
    
    pseq = [spt; lpt];
    coords = repmat([0; 1], 1, M);
    
    bdlx = {};
    bdly = {};
    bdli = {};
    for i = 1:M
        bdlx{i} = branch_data_list{i}(:,1);
        bdly{i} = branch_data_list{i}(:,2);
        bdli{i} = linspace(0, 1, lns(i));
    end
    
    for isp = 1:nsplits
        ncuts = size(pseq, 1) -1;

        npseq = [pseq(1,:)];
        npcoords = [coords(1,:)];

        for i = 1:ncuts
            pre = coords(i,:);
            post = coords(i+1,:);

            mpt = (pre(longest) + post(longest)) / 2;
            pre(longest) = [];
            post(longest) = [];
            
            a = (pre+post)/2;
            
            if 0
                fcost = @(a)scost(a, mpt, longest, bdli, bdlx, bdly, pprj);
                
                options = optimoptions('fmincon','Display','off');
                a = fmincon(fcost,a,[],[],[],[],pre,post,[],options);
                avec = [a(1:longest-1), mpt, a(longest:end)];
            else
                avec = (coords(i,:) + coords(i+1,:))/2;
            end
            
            npcoords(end+1,:) = avec;
            [~, X] = scost(a, mpt, longest, bdli, bdlx, bdly, pprj);
            npseq(end+1,:) = X;

            npcoords(end+1,:) = coords(i+1,:);
            npseq(end+1,:) = pseq(i+1,:);
        end

        coords = npcoords;
        pseq = npseq;
    end
    
    ndata = pseq;
end

function [err, X] = scost(a, mpt, longest, bdli, bdlx, bdly, pprj, return_just_X)
    avec = [a(1:longest-1), mpt, a(longest:end)];

    M = length(avec);
    pts = ones([M, 3]);
    for i = 1:M
        pts(i,1) = interp1(bdli{i},bdlx{i},avec(1));
        pts(i,2) = interp1(bdli{i},bdly{i},avec(1));
    end

    [X, xi] = reconstruct_point(pts, pprj);

    err = 0;
    for i = 1:M
        err = err + l2(xi(i,1:2) - pts(i,1:2));
    end
end

function data = get_branch_data(tree, bid)
    if length(bid) == 0
        data = tree.data;
    else
        data = get_branch_data(tree.children{bid(1)}, bid(2:end));
    end
end


function s = ebkey(frame_index, branch_id)
    s = sprintf('%d|%s', frame_index, nodekey(branch_id));
end

function s = nodekey(v)
    s = mat2str(v(:));
end