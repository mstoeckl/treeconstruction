function [imagesets, truth] = load_data(path, index)
    if nargin == 1
        folders = dir(path);
        dflgs = [folders.isdir];
        subfold = folders(dflgs);
        
        nimagesets = 0;
        for k = 1:length(subfold)
            if startsWith(subfold(k).name,'set')
                nimagesets = nimagesets + 1;
            end
        end
        
        imagesets = nimagesets;
    else
        subpath = sprintf('%s/set%d/',path,index-1);
        subfiles = dir(subpath);
        
        imagesets = {};
        for k = 1:length(subfiles)
            if startsWith(subfiles(k).name,'image')
                ij = sscanf(subfiles(k).name, 'image%d-%d.png');
                npath = strcat(subpath, subfiles(k).name);
                image = double(imread(npath)) / 255.0;
                imagesets{ij(2)+1} = image;
            end
        end

        subpath = sprintf('%s/set%d/meta%d.dat',path,index-1,index-1);
        fid = fopen(subpath, 'r');
    
        truth.filename = subpath;
        truth.camera_frame_model_mtxs = zeros([length(imagesets),4,4]);
        
        tree_parents = [];
        tree_curves = {};
        
        while true
            line = fgetl(fid);
            if ~ischar(line);
                break
            end
            
            if ~contains(line, '=')
                continue
            end
            
            sects = strsplit(line,'=');
            key = strtrim(sects{1});
            val = strtrim(sects{2});

            if strcmp(key,'projection_matrix')
                truth.camera_projection_mtx = parse_mat4x4(val);
            end
            
            if strcmp(key,'base_modelview_matrix')
                truth.camera_base_modelview_mtx = parse_mat4x4(val);
            end
            
            if startsWith(key,'modelview_matrix')
                i = sscanf(key, 'modelview_matrix%d') + 1;
                truth.camera_frame_model_mtxs(i,:,:) = parse_mat4x4(val);
            end
            
            if strcmp(key, 'view_angles')
                truth.camera_frame_angles = parse_li(val);
            end
            
            [keynum, status] = str2num(key);
            if status
                parts = strsplit(val,':');
                parent = str2num(strtrim(parts{1}));
                handles = strsplit(parts{2},';');
                
                evpts = zeros([0, 3]);
                for i = 1:length(handles)-1
                    [h1L, p1, h1R, ~] = parse_handle(handles{i});
                    [h2L, p2, h2R, ~] = parse_handle(handles{i+1});
                    
                    ss = evalbez(p1, h1R, h2L, p2, 20)';
                    if i > 1
                        ss = ss(2:end,:);
                    end
                    evpts = [evpts; ss];
                end
                
                tree_curves{keynum+1} = evpts;
                tree_parents(keynum+1) = parent+1;
            end
            
            
        end
        fclose(fid);

        % this adopts the child branch order from the metadata
        truth.tree = build_recursive_tree(1, tree_parents, tree_curves);
        fprintf('Loaded set %d, %d images, %d branches\n', index, length(imagesets), length(tree_curves));
    end
end

function array = parse_mat4x4(txt)
    sval = strsplit(txt, ';');
    array = zeros([4,4]);
    for i=1:4
        tvs = textscan(sval{i},'%f');
        array(:,i) = tvs{1};
    end
end

function v = parse_li(txt)
    tvs = textscan(txt,'%f');
    v = tvs{1};
end 

function [control_left, point, control_right, radius] = parse_handle(txt)
    sval = strsplit(txt, ',');
    sa = zeros([3,3]);
    for i = 1:3
        tvs = textscan(sval{i},'%f');
        sa(:,i) = tvs{1};
    end
    
    radius = textscan(sval{4},'%f');
    control_left = sa(:,1);
    point = sa(:,2);
    control_right = sa(:,3);
end

function pseq = evalbez(p1, h1R, h2L, p2, nt);
    t = linspace(0,1,nt);
    t = repmat(t,3,1);
    p1 = repmat(p1,1,nt);
    h1R = repmat(h1R,1,nt);
    h2L = repmat(h2L,1,nt);
    p2 = repmat(p2,1,nt);
    pseq = ((1 - t).^3) .* p1 + (3 .* t .* (1 - t).^2) .* h1R + (3 .* (t.^2) .* (1 - t)) .* h2L + (t.^3) .* p2;
end