function tree = build_recursive_tree(current_index, parents, curves)
    tree.data = curves{current_index};
    tree.children = {};
    
    csel = find(parents == current_index);
    for i = 1:length(csel)
        tree.children{i} = build_recursive_tree(csel(i), parents, curves);
    end
end 
