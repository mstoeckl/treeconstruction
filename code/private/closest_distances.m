function d = closest_distances(pl)
    pl = pl(:,1:2);
    
    % closest distance to each point
    kd = createns(pl);
    [idx,d] = knnsearch(kd,pl,'K',4);
    
    d(d<=1e-10) = 1e99;
    d = min(d, [], 2);
end
