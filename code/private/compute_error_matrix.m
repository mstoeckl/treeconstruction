function errs = compute_error_matrix(hpA, hpB, pointsA, pointsB, sigmaA, sigmaB, pI)
    hp = vertcat(hpA, hpB);
    
    nA = length(pointsA);
    nB = length(pointsB);
    errs = inf([nA, nB]);

    for i = 1:nA
        for j = 1:nB
            qA = pointsA(i,:);
            qB = pointsB(j,:);
            Z = vertcat(crossmtx(qA) * hpA, crossmtx(qB) * hpB);
            
            [~, s, v] = svd(Z);
            [~, vi] = sort(abs(diag(s)));
            X = v(:, vi(1));
            X = X ./ X(4);
            
            predicted = hp * X;
            if abs(predicted(6)) <= 1e-10 || abs(predicted(3)) <= 1e-10
                continue
            end

            zA = predicted(1:2) / predicted(3);
            zB = predicted(4:5) / predicted(6);
            rA = qA(1:2)';
            rB = qB(1:2)';

            dA = l2(zA - rA);
            dB = l2(zB - rB);

            err = min(dA.^2, sigmaA(i).^2) + min(dB.^2, sigmaB(j).^2);

            errs(i, j) = err;
        end
    end
end
