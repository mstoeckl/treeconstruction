function mtx = get_pixel_transform(imsize)
    w = imsize(1);
    h = imsize(2);
    mtx = [0, -w/2, 0, w/2; h/2, 0, 0, h/2; 0, 0, 0, 1];
end
