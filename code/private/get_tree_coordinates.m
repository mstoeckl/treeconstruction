function tc = get_tree_coordinates(tree, path_to)
    if nargin == 1
        path_to = [];
    end
    
    tc = {cat(1, path_to, [-2]), cat(1, path_to, [-1])};
    
    for i = 1:length(tree.children)
        tc = [tc, get_tree_coordinates(tree.children{i}, cat(1, path_to, [i]))];
    end
end