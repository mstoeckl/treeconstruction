function s = l2(v)
    s = sqrt(sum(v.^2));
end 
