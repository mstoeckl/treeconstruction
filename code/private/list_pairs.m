function [pairs, npairs] = list_pairs(n)
    pairs = zeros([0,2],'int32');
    for i=1:n
        for j=1:i-1
            pairs(end+1,:) = [i,j];
        end
    end
    npairs = size(pairs, 1);
end