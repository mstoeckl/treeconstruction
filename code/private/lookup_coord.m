function data = lookup_coord(coord, tree)
    if coord(1) < 0
        if coord(1) == -1
            data = tree.data(1,:);
        elseif coord(1) == -2
            data = tree.data(end,:);
        end
    else
        data = lookup_coord(coord(2:end), tree.children{coord(1)});
    end
end
