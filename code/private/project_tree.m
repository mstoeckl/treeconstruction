function tree2d = project_tree(tree, mvp, imagesize)
    fourth = ones([size(tree.data, 1), 1]);
    pts = [tree.data, fourth];
    
    pts = (mvp * pts')';
    
    pts = pts(:,1:2) ./ pts(:,4);
    pts = fliplr(pts);
    pts = pts + [1,1];
    pts = pts .* (imagesize ./ 2);
    pts(:,1) = imagesize(1) - pts(:,1);
    
    tree2d.data = pts;
    tree2d.children = {};
    for i = 1:length(tree.children)
        tree2d.children{i} = project_tree(tree.children{i}, mvp, imagesize);
    end
end 
