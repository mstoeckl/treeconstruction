% reconstruction algorithm divided into steps

fake_2d_fit = false;
fake_frames = false;
run_scoring = false;

path = '../data/generated/';
if ~exist('gen', 'dir')
    status = mkdir('gen');
end

nsets = load_data(path);
for i = 1:nsets
    [imvec, truth] = load_data(path, i);
    imsizes = zeros([0,2]);
    for j = 1:length(imvec)
        [w,h,~] = size(imvec{j});
        imsizes(j,:) = [w,h];
    end
    vector_data = {};
    if ~fake_2d_fit
        for j = 1:length(imvec)
            mask = detect_foreground(imvec{j});
            vector_data{j} = vectorize_mask(mask);
        end
    else
        for j = 1:length(imvec)
            projection = truth.camera_projection_mtx * squeeze(truth.camera_frame_model_mtxs(j,:,:));
            vector_data{j} = project_tree(truth.tree, projection, imsizes(j,:));
        end
    end
    
    if ~fake_frames
        frames = find_camera_frames(vector_data, truth, imsizes);
    else
        frames = {};
        for j = 1:length(imvec)
            frames{j} = truth.camera_projection_mtx * squeeze(truth.camera_frame_model_mtxs(j,:,:));
        end
    end
    tree = find_tree(frames, vector_data, imsizes);
    for j = 1:length(frames)
        save_path = sprintf('gen/show%d-%d', i, j);
        show_tree(imvec{j}, frames{j}, tree, vector_data, truth, j, save_path);
    end
    
    if run_scoring
        score_reconstruction(tree, truth);
    end
end