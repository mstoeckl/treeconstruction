function score_reconstruction(treeA, treeB)

%trees are an n x 3 set of arrays where a point is given by x,y,z

%the output is an average distance from each point in the reconstructed
%tree to the nearest point in the truth tree

[mA,nA] = size(treeA);
[mB, nB] = size(treeB);


%score = pdist2(treeA, treeB, 'euclidean');
score = 0;

for i = 1 : 1 : mA
    %iterate through all the points in the tree we reconstructed
    
    xa = treeA(i,1);
    ya = treeA(i,2);
    za = treeA(i,3);
    
    min = -1;
    
    %iterate through all the points in truth to find the closest one
    for j = 1 : 1 : mB
    
        xb = treeB(j,1);
        yb = treeB(j,2);
        zb = treeB(j,3);
    
        %distance from the point in A to the point in b
        dist = sqrt((xa - xb).^2 + (ya - yb).^2 + (za - zb).^2);
        
        if min == -1
            
            min = dist;
            
        else
            
            %if the current point is closer set the min to be this distance
            if min > dist
                
                min = dist;
                
            end
            
        end
        
    end
    
    %add the minimum distance to the score
    score = score + min;
        
end

%average the score
score = score./mA;

fprintf(score);

