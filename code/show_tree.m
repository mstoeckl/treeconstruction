function show_tree(image, frame, tree, vector_data, truth, index, path)
    clf('reset');
    % show image and projected tree
    fig = gcf;
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = [0 0 30 30];

    %  imagesc(detect_foreground(image));
    
    proj = truth.camera_projection_mtx;
    modelview = squeeze(truth.camera_frame_model_mtxs(index,:,:));
    
    mvp = proj * modelview;
    [a,b,~] = size(image);
    ptree = project_tree(truth.tree, mvp, [a,b]);
    
    lines = get_tree_lines(ptree);
    
    lab = rgb2lab(image);
    imagesc(lab(:,:,3));
    hold on;
    axis equal;
    axis square;
    for i = 1:length(lines)
        l = lines{i};
        plot(l(:,2), l(:,1),'Color',[1.0,0.3,0.1])
    end
%      
%      alines = get_tree_lines(vector_data{index});
%      for i = 1:length(alines)
%          l = alines{i};
%          plot(l(:,2), l(:,1),'Color',[0.0,1.0,0.0])
%      end
%      
    
    qtree = project_tree(tree, frame, [a,b]);
    qlines = get_tree_lines(qtree);
    for i = 1:length(qlines)
        l = qlines{i};
        plot(l(:,2), l(:,1),'Color',[0.0,0.0,1.0])
    end

    title([path, ' ', datestr(now,'yy-dd-mm HH:MM:SS.FFF')]);
    
    ax = fig.CurrentAxes;
    ax.OuterPosition = [0 0 1 1];
    ax.Position = [0.05 0.05 0.9 0.9];

    print(path,'-dpng','-r300')
end

function lines = get_tree_lines(tree2d);
    lines = {tree2d.data};
    
    for i = 1:length(tree2d.children)
        lines = [lines; get_tree_lines(tree2d.children{i})];
    end
end

function draw_tree(tree2d, truth, index)
    plot(tree2d.data(:,1), tree2d.data(:,2))
    
    for i = 1:length(tree2d.children)
        draw_tree(tree2d.children{i});
    end
end