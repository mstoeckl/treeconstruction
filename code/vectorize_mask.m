function vector_repr = vectorize_mask(mask)

    mask = fast_iterative_thinning(mask);
    mask = reduce_8_to_4_connected(mask);

    [vertex_neighs, segments, start_coords, nendpoints] = extract_graph(mask);

    root_index = identify_root_index(start_coords);

    path_bundle = identify_path_bundles(vertex_neighs, segments, start_coords, root_index, nendpoints);

    [hyperedges, hypervertedges] = compute_hypergraph(path_bundle, segments, root_index);

    tree = graph_to_tree(hyperedges, hypervertedges);

    tree = convert_to_branch_form(tree);

    tree = smooth_tree(tree, mask);

    fprintf('Have %d segments, %d paths, %d hyperedges, %d branches\n', ...
        length(segments), length(path_bundle),length(hyperedges),tree_count(tree));
    
    vector_repr = tree;
end

function mask = reduce_8_to_4_connected(imask)
    msk = [-1 1 -1; -1 1 1; 0 -1 -1];
    mask = imask;

    for i = 1:4
        fg = rot90(msk == 1, i);
        bg = rot90(msk == 0, i);
        hm = bwhitmiss(mask, fg, bg);
        mask(hm) = false;
    end
end

function mask = fast_iterative_thinning(imask)
    % Implements [Zhang & Suen, 1984]; note OPATA8 might be better
    
    [a,b] = size(imask);
    mask = zeros([a+2,b+2],'logical');
    mask(2:end-1,2:end-1) = imask;
    
    neighbors8 = [-1 1; 0 1; 1 1; 1 0; 1 -1; 0 -1; -1 -1; -1 0];
    
    [x,y] = find(mask);
    roi = [x'; y']';
    
    cull_counts = [];
    for iteration = 1:max(a,b)
        [nroi, ~] = size(roi);
        nmap = zeros([nroi, 8], 'logical');
        for k = 1:8
            sil = sub2ind(size(mask), roi(:, 1) + neighbors8(k,1), roi(:, 2) + neighbors8(k,2));
            nmap(:, k) = mask(sil);
        end

        B = sum(nmap, 2);
        A = sum(nmap < circshift(nmap,1,2), 2);

        valB = (2 <= B) & (B <= 6);
        valA = (A == 1);

        if mod(iteration, 2) == 0
            zp246 = ~(nmap(:, 2) & nmap(:, 4) & nmap(:, 6));
            zp468 = ~(nmap(:, 4) & nmap(:, 6) & nmap(:, 8));

            to_cull = valA & valB & zp246 & zp468;
        else
            zp248 = ~(nmap(:, 2) & nmap(:, 4) & nmap(:, 8));
            zp268 = ~(nmap(:, 2) & nmap(:, 6) & nmap(:, 8));
            to_cull = valA & valB & zp248 & zp268;
        end

        ncull = sum(to_cull);
        cull_counts(end+1) = ncull;

        if length(cull_counts) >= 2 && cull_counts(end) == 0 && cull_counts(end - 1) == 0
            break
        end

        kil = sub2ind(size(mask),roi(to_cull, 1), roi(to_cull, 2));
        mask(kil) = false;
        roi = roi(~to_cull,:);
    end
    
    mask = mask(2:end-1,2:end-1);

end

function [vertex_neighs, segments, start_coords, nendpoints] = extract_graph(mask)
    masksize = size(mask);
    [eps, jps] = find_keypoints(mask);

    [labelled_endpoints, nendpoints] = bwlabel(eps);
    [labelled_junctions, njunctions] = bwlabel(jps);
    
    start_coords = zeros([nendpoints + njunctions, 2]);
    
    [Y,X] = meshgrid(1:masksize(2),1:masksize(1));
    O = ones(masksize, 'int32');
    exs = accumarray(labelled_endpoints(:)+1,X(:));
    eys = accumarray(labelled_endpoints(:)+1,Y(:));
    eos = accumarray(labelled_endpoints(:)+1,O(:));
    
    start_coords(1:nendpoints, 1) = exs(2:end) ./ eos(2:end);
    start_coords(1:nendpoints, 2) = eys(2:end) ./ eos(2:end);
    
    jxs = accumarray(labelled_junctions(:)+1,X(:));
    jys = accumarray(labelled_junctions(:)+1,Y(:));
    jos = accumarray(labelled_junctions(:)+1,O(:));
    
    start_coords(nendpoints+1:end, 1) = jxs(2:end) ./ jos(2:end);
    start_coords(nendpoints+1:end, 2) = jys(2:end) ./ jos(2:end);
    
    vertex_neighs = cell([nendpoints + njunctions, 1]);
    for i = 1:nendpoints + njunctions
        vertex_neighs{i} = zeros([0,1],'int32');
    end
    segments = struct('u',[],'v',[],'data',{});
    
    reached = zeros(masksize, 'logical');
    vertmp = eps | jps;
    labelled_junctions(labelled_junctions > 0) = labelled_junctions(labelled_junctions > 0) + nendpoints;
    
    point_indices = labelled_junctions;
    point_indices(labelled_endpoints > 0) = labelled_endpoints(labelled_endpoints > 0);
    
    je = ordfilt2(labelled_junctions,9,true(3));
    me = ordfilt2(labelled_endpoints,9,true(3));
    
    exptidcs = je;
    exptidcs(me > 0) = me(me > 0);
    exptidcs(je > 0 & me > 0) = 0;
    
    
    neighbors8 = [-1 1; 0 1; 1 1; 1 0; 1 -1; 0 -1; -1 -1; -1 0];

    seeds = find(vertmp);
    for oi = 1:length(seeds)
        while true
            [lx,ly] = ind2sub(masksize, seeds(oi));
            start_type = point_indices(lx, ly);
            assert(start_type ~= 0);
            
            nopts = zeros([8,1],'logical');
            vton = zeros([8,1],'logical');
            for i=1:8
                sx = lx + neighbors8(i,1);
                sy = ly + neighbors8(i,2);
                if 1 <= sx && sx <= masksize(1) && 1 <= sy && sy <= masksize(2) && mask(sx,sy) && ~reached(sx,sy) && point_indices(sx,sy) ~= start_type
                    nopts(i) = true;
                    vton(i) = vertmp(sx, sy);
                end
            end
            
            if sum(nopts) <= 0
                break
            end

            novt = nopts & ~vton;
            wvt = nopts & vton;
            if sum(sum(wvt)) > 0
                i = find(wvt);
            else
                i = find(novt);
            end
            i = i(end);
            
            nx = lx + neighbors8(i,1);
            ny = ly + neighbors8(i,2);
            lpts = [lx, ly; nx, ny];
            reached(nx,ny) = true;
            
            loop_path = false;
            while true
                lx = lpts(end, 1);
                ly = lpts(end, 2);
                
                if vertmp(lx, ly)
                    end_type = point_indices(nx, ny);
                    break
                end
                
                nopts = zeros([8,1],'logical');
                vton = zeros([8,1],'logical');
                for i=1:8
                    sx = lx + neighbors8(i,1);
                    sy = ly + neighbors8(i,2);
                    if sx == lpts(end - 1, 1) && sy == lpts(end - 1, 2)
                        continue
                    end
                    
                    if 1 <= sx && sx <= masksize(1) && 1 <= sy && sy <= masksize(2) && mask(sx,sy) && exptidcs(sx,sy) ~= start_type %&& ~reached(sx,sy)
                        nopts(i) = true;
                        vton(i) = vertmp(sx, sy);
                    end
                end
                
                if sum(sum(nopts)) <= 0
                    loop_path = true;
                    break
                end

                novt = nopts & ~vton;
                wvt = nopts & vton;
                if sum(sum(wvt)) > 0
                    i = find(wvt);
                else
                    i = find(novt);
                end
                i = i(end);
                
                nx = lx + neighbors8(i,1);
                ny = ly + neighbors8(i,2);
                [nls, ~] = size(lpts);
                lpts(nls+1, :) = [nx, ny];
                %reached(nx, ny) = true;
                
                [u,I,J] = unique(lpts, 'rows', 'first');
                hasDuplicates = size(u,1) < size(lpts,1);
                if hasDuplicates
                    lpts
                    error('has duplicates');
                end
            end
            
            if loop_path
                continue
            end
            
            [nls, ~] = size(lpts);
            for i = 2:nls-1
                reached(lpts(i,1),lpts(i,2)) = true;
            end
            
            idx = length(segments) + 1;
            vertex_neighs{start_type}(end + 1) = idx;
            vertex_neighs{end_type}(end + 1) = idx;
            lpts = [start_coords(start_type,:); lpts(2:end-1,:); start_coords(end_type, :)];
            
            segments(idx).u = start_type;
            segments(idx).v = end_type;
            segments(idx).data = lpts;
        end
    end
end

function root_index = identify_root_index(start_coords)
    record = -Inf;
    [l, ~] = size(start_coords);
    for i = 1:l
        if start_coords(i,1) > record
            record = start_coords(i,1);
            root_index = i;
        end
    end
end

function [eps, jps] = find_keypoints(mask)
    junct_masks = [-1 1 -1; 0 1 0; 1 0 1];
    junct_masks(:,:,2) = [1 0 -1; 0 1 0; 1 0 1];
    junct_masks(:,:,3) = [-1 0 1; 1 1 0; 0 1 0];
    
    ep_masks = [-1 1 -1; 0 1 0; 0 0 0];
    ep_masks(:,:,2) = [1 0 0; 0 1 0; 0 0 0];
        
    eps = roto_hitmiss(mask, ep_masks);
    jps = roto_hitmiss(mask, junct_masks);
end

function res = roto_hitmiss(mask, operators)
    res = zeros(size(mask), 'logical');
    [u,v,w] = size(operators);
    for i = 1:w
        for k = 1:4
            jm = squeeze(rot90(operators(:,:,i),k));
            fg = (jm == 1);
            bg = (jm == 0);
            up = bwhitmiss(mask, fg, bg);
            res = res | up;
        end
    end
end


function y = opposing_index(segment, x)
    if segment.u == x
        y = segment.v;
    elseif segment.v == x
        y = segment.u;
    else
        error('Neither index matches');
    end
end

function length = path_length(ps)
    deltas = ps(2:end,:) - ps(1:end-1,:);
    sublengths = sum((deltas.^2)')'.^0.5;
    length = sum(sublengths);
end

function nseg = flipseg(seg) 
    nseg.u = seg.v;
    nseg.v = seg.u;
    nseg.data = flipud(seg.data);
end

function angles = angles_rel_key(celloseg, key)
    dirvecs = zeros([length(celloseg),2]);
    for i = 1:length(celloseg)
        dirvecs(i,:) = celloseg{i}.data(2,:) - celloseg{i}.data(1,:);
    end
    angles = atan2(dirvecs(:,2),dirvecs(:,1));
    angles = mod(angles + 2 * pi - angles(key), 2*pi);
    angles(key) = 0.;
end

function sel = pick_unselected(seq, chosen)
    o = ones([length(seq),1],'logical');
    o(chosen) = false;
    i = find(o);
    sel = i(1);
end

function best_bundle = identify_path_bundles(vertex_neighs, segments, start_coords, root_index, nendpoints)

    npts = length(vertex_neighs);
    distance_to_root = inf([npts, 1]);
    distance_to_root(root_index) = 0.;
    
    unvisited = containers.Map(1:npts, ones([npts, 1],'logical'));
    remove(unvisited, root_index);
    
    while length(unvisited)
        found = false;
        ukeys = keys(unvisited);
        for iu = 1:length(ukeys)
            u = ukeys{iu};
            edge_ids = vertex_neighs{u};
            rec = Inf;
            for k = 1:length(edge_ids)
                eid = edge_ids(k);
                edge = segments(eid);
                v = opposing_index(edge, u);
                if distance_to_root(v) >= Inf
                    continue
                end

                cost = distance_to_root(v) + path_length(edge.data);
                rec = min(cost, rec);
            end

            if rec < Inf
                distance_to_root(u) = rec;
                remove(unvisited, u);
                found = true;
                break
            end
        end

        if ~found
            distance_to_root
            idxs = find(distance_to_root >= Inf)
            for i=1:length(idxs)
                s = vertex_neighs(idxs(i))
                start_coords(idxs(i),:)
            end
            error('Graph should have been a tree');
        end
    end
    
    min_leftover_length = Inf;
    
    ntrials = 10;
    
    for trial = 1:ntrials
        path_bundle = {};
        for i = 1:nendpoints
            current_vertex = i;
            edges = [];
            
            last_branch_decision = 'N';
            while current_vertex ~= root_index
                edge_ids = vertex_neighs{current_vertex};
                
                ops = zeros([length(edge_ids),1]);
                for i = 1:length(edge_ids)
                    ops(i) = opposing_index(segments(edge_ids(i)), current_vertex);
                end
                
                optc = distance_to_root(ops) < distance_to_root(current_vertex);

                if sum(optc) == 2 && length(ops) == 3 && last_branch_decision ~= 'N'
                    is_infeed = ~optc;
                    assert(sum(is_infeed) == 1);
                    
                    key = find(is_infeed);
                    key = key(1);
                    
                    ss = cell([3,1]);
                    for i=1:3
                        if segments(edge_ids(i)).u ~= current_vertex
                            ss{i} = flipseg(segments(edge_ids(i)));
                        else
                            ss{i} = segments(edge_ids(i));
                        end
                    end

                    angles = angles_rel_key(ss, key);

                    [~, maxi] = max(angles);
                    alt = pick_unselected(1:3, [maxi, key]);
                    
                    if last_branch_decision == 'R'
                        v = ops(maxi);
                        e = edge_ids(maxi);
                    elseif last_branch_decision == 'L'
                        v = ops(alt);
                        e = edge_ids(alt);
                    else
                        error('Invariant fail');
                    end
                else
                    ids = find(optc);
                    r = randi(length(ids));
                    v = ops(ids(r));
                    e = edge_ids(ids(r));
                end

                current_vertex = v;
                edges(end+1) = e;

                if length(vertex_neighs{current_vertex}) == 3
                    edge_ids = vertex_neighs{current_vertex};
                    ops = zeros([length(edge_ids),1]);
                    for i = 1:length(edge_ids)
                        ops(i) = opposing_index(segments(edge_ids(i)), current_vertex);
                    end

                    is_infeed = distance_to_root(ops) >= distance_to_root(current_vertex);

                    if sum(is_infeed) == 2
                        ss = cell([3,1]);
                        for i=1:3
                            if segments(edge_ids(i)).u ~= current_vertex
                                ss{i} = flipseg(segments(edge_ids(i)));
                            else
                                ss{i} = segments(edge_ids(i));
                            end
                        end
                        
                        key = find(is_infeed);
                        key = key(1);
                        test = find(edge_ids == e);
                        test = test(1);
                        alt = pick_unselected(1:3, [test, key]);

                        angles = angles_rel_key(ss, key);

                        if angles(test) < angles(alt)
                            last_branch_decision = 'L';
                        else
                            last_branch_decision = 'R';
                        end
                    end 
                end
            end
            path_bundle{end + 1} = fliplr(edges);
        end
        
        used = zeros([length(segments),1],'logical');
        for i = 1:length(path_bundle)
            pi = path_bundle{i};
            for j = 1:length(pi)
                used(pi(j)) = true;
            end
        end
        
        netlength = 0;
        for i=1:length(used)
            if ~used(i)
                netlength = netlength + path_length(segments(i).data);
            end
        end
        
        if netlength < min_leftover_length
            min_leftover_length = netlength;
            best_bundle = path_bundle;
        end
    end
end

function s = char_key(path)
    s = mat2str(path(:));
end

function sseg = merge_segments(segs)
    n = length(segs);
    sids = zeros([n,2],'int32');
    for i=1:n
        sids(i,1) = segs(i).u;
        sids(i,2) = segs(i).v;
    end
    
    if n > 1
        fsids = sids;
        for i = 1:n-1
            if fsids(i,2) ~= fsids(i+1,1) && fsids(i,2) ~= fsids(i+1,2)
                fsids(i,:) = fliplr(fsids(i,:));
            end
        end
        if fsids(end, 1) ~= fsids(end-1,1) && fsids(end, 1) ~= fsids(end-1,2)
            fsids(end,:) = fliplr(fsids(end,:));
        end 
    else
        fsids = sids;
    end
    
    flipped = zeros([n,1]);
    for i=1:n
        flipped(i) = (sids(i,1) == fsids(i,2));
    end
    
    if flipped(1)
        sedge = flipud(segs(1).data);
    else
        sedge = segs(1).data;
    end
    
    for i=2:n
        if flipped(i)
            ap = flipud(segs(i).data);
        else
            ap = segs(i).data;
        end
        ap = ap(2:end,:);
        sedge = [sedge; ap];
    end
    
    sseg.u = fsids(1,1);
    sseg.v = fsids(end,2);
    sseg.data = sedge;
end

function [hyperedges, hypervertedges] = compute_hypergraph(path_bundle, segments, root_index)
    hyperbranches = containers.Map();
    hyperterminal = containers.Map();
    hyperpaths = containers.Map();
    
    blank = zeros([0,1],'int32');
    hyperterminal(char_key(blank)) = true;

    for k=1:length(path_bundle)
        path = path_bundle{k};
        for i = 1:length(path)
            key = char_key(path(1:i-1));
            if ~isKey(hyperbranches, key)
                hyperbranches(key) = [];
            end
            s = hyperbranches(key);
            s(end+1) = path(i);
            hyperbranches(key) = s;
            hyperpaths(key) = path(1:i-1);
        end 
        hyperpaths(char_key(path)) = path;
        hyperbranches(char_key(path)) = [];
        hyperterminal(char_key(path)) = true;
    end
    
    hypernodes = containers.Map();
    sk = keys(hyperpaths);
    for i=1:length(sk)
        k = sk{i};
        endnode = isKey(hyperterminal, k);
        splitnode = length(hyperbranches(k)) >= 2;
        if endnode || splitnode
            path = hyperpaths(k);
            hypernodes(char_key(path)) = path;
        end
    end
    
    hyperends = containers.Map();
    sk = keys(hypernodes);
    for i=1:length(sk)
        node = hypernodes(sk{i});
        start = root_index;
        for ii = 1:length(node)
            eid = node(ii);
            start = opposing_index(segments(eid), start);
        end
        hyperends(char_key(node)) = start;
    end
    
    hyperedges = struct('u',{},'v',{},'data',{});
    hypervertedges = containers.Map();
    for i=1:length(sk)
        node = hypernodes(sk{i});
        if length(node) <= 0
            continue
        end
        
        for k = length(node):-1:1
            if isKey(hypernodes, char_key(node(1:k-1)))
                from_node = node(1:k-1);
                skipped = node(k:end);
                break
            end
        end
        
        if length(from_node) <= 0
            from_node = blank;
        end

        sseg = merge_segments(segments(skipped));
        
        if sseg.u ~= hyperends(char_key(from_node))
            sseg = flipseg(sseg);
        end

        assert(sseg.u == hyperends(char_key(from_node)))
        assert(sseg.v == hyperends(char_key(node)))

        superseg.u = from_node;
        superseg.v = node;
        superseg.data = sseg.data;

        idx = length(hyperedges) + 1;
        hyperedges(idx) = superseg;
        ukey = char_key(superseg.u);
        vkey = char_key(superseg.v);
        if ~isKey(hypervertedges, ukey)
            hypervertedges(ukey) = [];
        end
        if ~isKey(hypervertedges, vkey)
            hypervertedges(vkey) = [];
        end
        hypervertedges(ukey) = union([idx],hypervertedges(ukey));
        hypervertedges(vkey) = union([idx],hypervertedges(vkey));
    end
end

function count = tree_count(tree)
    count = 1;
    for i=1:length(tree.children)
        count = count + tree_count(tree.children{i});
    end
end

function tree = graph_to_tree(hyperedges, hypervertedges)
    function subtree = recfill(sid, from_node)
        segment = hyperedges(sid);
    
        if isequal(segment.v, from_node)
            segment = flipseg(segment);
        end
        assert(isequal(segment.u, from_node));

        to_node = segment.v;

        eids = hypervertedges(char_key(to_node));

        eids = setdiff(eids, [sid]);
        
        subtree.data = segment.data;
        
        subtree.children = {};
        for ie = 1:length(eids)
            subtree.children{ie} = recfill(eids(ie), to_node);
        end
    end

    hyperroot = zeros([0,1],'int32');
    for i=1:length(hyperedges)
        if isequal(hyperedges(i).u, hyperroot) || isequal(hyperroot, hyperedges(i).v)
            start_edge = i;
        end
    end

    tree = recfill(start_edge, hyperroot);
    assert(tree_count(tree) == length(hyperedges));
end

function score = align_score(segA, segB)
    assert(isequal(segA(end,:), segB(1,:)));

    Avec = segA(end,:) - segA(1,:);
    Bvec = segB(end,:) - segB(1,:);

    costheta = (Avec * Bvec') / (sum(Avec.^2) * sum(Bvec.^2)).^0.5;
    score = costheta;
end

function branch_tree = convert_to_branch_form(topo_tree)
    function btree = branchify(tree)
        kids = {};
        path = tree.data;
        current = tree;
        
        while length(current.children)
            alignments = [];
            for i=1:length(current.children)
                alignments(i) = align_score(path, current.children{i}.data);
            end
            [~, order] = sort(alignments, 'descend');
            
            children = current.children(order);
            nk = length(kids);
            for i=2:length(children)
                kids{i+nk-1} = children{i};
            end
            
            current = children{1};
            assert(isequal(path(end,:), current.data(1,:)));
            path = [path(1:end-1,:); current.data];
        end

        if length(kids)
            kvs = zeros([length(kids),2]);
            for i=1:length(kids)
                kvs(i,:) = kids{i}.data(1,:);
            end
            
            [~, locb] = ismember(kvs,path,'rows');
            assert(length(locb) == length(kids));
            [~, order] = sort(locb);
            kids = kids(order);
        end

        btree.children = {};
        for i =1:length(kids)
            btree.children{i} = branchify(kids{i});
        end
        btree.data = path;
    end

    branch_tree = branchify(topo_tree);
end

function smp = smooth3(ptseq)
    smp = [ptseq(1,:); (ptseq(1:end-2,:) + ptseq(2:end-1,:) + ptseq(3:end,:)) / 3.; ptseq(end,:)];
end
    
function stree = smooth_tree(tree, mask)
    function mod = recsmooth(node)
        mod.data = smooth3(node.data);
        mod.children = {};
        for i = 1:length(node.children)
            mod.children{i} = recsmooth(node.children{i});
        end
    end

    stree = recsmooth(tree);
end