#!/usr/bin/env python3

"""
The script is typically imported
"""

import bpy
import time, sys, collections, os, random, math

import sapling_gen

# Set render target
cpath = os.path.abspath('.')
print("Current path", cpath)

os.makedirs('generated', exist_ok=True)

for index in range(1000):
    cand = cpath + '/generated/set{}/'.format(index)
    if not os.path.exists(cand):
        os.makedirs(cand)
        break
        
metafile = cpath + '/generated/set{}/meta{}.dat'.format(index,index)

mode = 'quaking_aspen'
#mode = 'ca_black_oak'

props = eval(open('presets/{}.py'.format(mode)).read())

props['leafShape'] = 'hex'
props['bevel'] = True
props['bevelRes'] = 8
props['showLeaves'] = False
props['seed'] = random.randint(0, 2**30)
props['levels'] = 2
## TODO: enforce visible ends!
props['taper'] = (0.95, 0.8, 0.8, 0.8)

TSpec = collections.namedtuple("treespec", props.keys())

# Generate tree

bpy.context.scene.cursor_location = (0.0, 0.0, 0.0)
sapling_gen.addTree(TSpec(**props))

treemat = bpy.context.scene.objects['Vert'].data.materials[0]
# print(matsource.data.materials[0])
treemat = bpy.data.materials.get("TreeBark")
bpy.context.scene.objects['tree'].data.materials.append(treemat)

if True:
    bpy.ops.wm.save_as_mainfile(filepath=cpath +
        '/generated/set{}/model{}.blend'.format(index,index))

# Record tree information

treeobj = bpy.context.scene.objects['tree']
splinelist = treeobj.data.splines
cameraobj = bpy.context.scene.objects['Camera']

nimages = random.randint(2,5)
view_angles = [random.random()*2*math.pi for i in range(nimages)]

import mathutils
npts = 100
# Create a closest-point lookup tree. 
# Better would be a closest-point-to-spline primitive, filtered by rectangle tree
nmaxpts = npts * sum(len(spline.bezier_points) for spline in splinelist)
kd = mathutils.kdtree.KDTree(nmaxpts)

for kkk, spline in enumerate(splinelist):
    for i in range(len(spline.bezier_points) - 1):
        knot1 = spline.bezier_points[i].co
        handle1 = spline.bezier_points[i].handle_right
        handle2 = spline.bezier_points[i + 1].handle_left
        knot2 = spline.bezier_points[i + 1].co

        plist = mathutils.geometry.interpolate_bezier(knot1, handle1, handle2,
            knot2, npts)
        for point in plist:
            kd.insert(point, kkk)
kd.balance()

def fmtp(v3):
    return "{:.6g} {:.6g} {:.6g}".format(v3.x, v3.y, v3.z)

origin = bpy.context.scene.objects['Pivot']

with open(metafile, 'w') as mf:
    print("[Tree properties]", file=mf)
    for k, v in props.items():
        print(k, '=', v, file=mf)

    print("\n[Camera initial properties]", file=mf)
    print('location =', fmtp(cameraobj.location), file=mf)
    print('rotation_euler =', fmtp(cameraobj.rotation_euler), file=mf)
    print('scale =', fmtp(cameraobj.scale), file=mf)
    print('view_angles =', *[str(f) for f in view_angles],file=mf)
    
    r = bpy.context.scene.render
    projection_matrix = bpy.context.scene.camera.calc_matrix_camera(
        r.resolution_x,
        r.resolution_y,
        r.pixel_aspect_x,
        r.pixel_aspect_y)
    def mat_to_text(mat):
        return '; '.join([' '.join([str(mat[i][j]) for i in range(4)]) for j in range(4)])
    print("projection_matrix =", mat_to_text(projection_matrix), file=mf)
    
    base_modelview_matrix = bpy.context.scene.camera.matrix_world.inverted()
    print("base_modelview_matrix =".format(i), mat_to_text(base_modelview_matrix), file=mf)
    
    for i in range(len(view_angles)):
        origin.rotation_euler[2] = view_angles[i]
        bpy.context.scene.update()
        modelview_matrix = bpy.context.scene.camera.matrix_world.inverted()
        print("modelview_matrix{} =".format(i), mat_to_text(modelview_matrix), file=mf)
    print("\n[Tree]", file=mf)
    
    for i, spline in enumerate(splinelist):
        spline_repr = []
        k = 10
        closest_idx = -1
        if i != 0:
            nearest = kd.find_n(spline.bezier_points[0].co, k)
            valid = [(v, idx, d) for (v, idx, d) in nearest if idx != i]
            if len(valid):
                (closest_point, closest_idx, closest_dist) = min(valid,
                    key=lambda s: s[2])

        for point in spline.bezier_points:
            p_repr = [fmtp(point.handle_left), fmtp(point.co),
                fmtp(point.handle_right), "{:.6g}".format(point.radius)]

            spline_repr.append(', '.join(p_repr))

        print(i, '=', repr(closest_idx) + ':', '; '.join(spline_repr), file=mf)


for i in range(len(view_angles)):
    origin.rotation_euler[2] = view_angles[i]
    bpy.context.scene.render.filepath = cpath + '/generated/set{}/image{}-{}.png'.format(index,index,i)
    bpy.ops.render.render(write_still=True) 


