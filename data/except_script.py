#!/usr/bin/env python3

""" This file exists to catch all errors in autogen, even SyntaxError """

if __name__ == '__main__':
    import sys, os
    sys.path.append(os.path.abspath('.'))
    try:
        import autogen 
    except:
        import traceback
        traceback.print_exc()
        sys.exit(1)
