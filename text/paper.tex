\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{units}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=true,hidelinks]{hyperref}

\cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ifcvprfinal\pagestyle{empty}\fi
\setcounter{page}{1}
\begin{document}

%%%%%%%%% TITLE
\title{Leafless tree shape estimation from few images}

\author{Manuel Stoeckl\\
University of Rochester\\
500 Joseph C. Wilson Blvd., Rochester, NY 14627\\
{\tt\small mstoeckl@u.rochester.edu}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Stephen Watson\\
Institute of Optics, University of Rochester\\
500 Joseph C. Wilson Blvd., Rochester, NY 14627\\
{\tt\small swatson8@u.rochester.edu}
}

\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
A standard problem in computer vision is to create a 3D model of an object 
given multiple images. Restricting the scope of this task to leafless trees 
permits faster and more accurate solutions. This paper introduces a method for 
reconstructing 3D tree model given images taken from multiple angles. The 
proposed algorithm estimates 2D tree structures for each image and from them 
determines camera frames and then fits a 3D tree structure. The algorithm is 
tested on a synthetic dataset rendered using Blender, and reconstruction 
quality is found to improve with the number of images. 

\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}

We create an algorithm that reconstructs a 3D leafless tree from multiple 
images taken at different angles. Using a data set created with Blender, the 
algorithm determines key points along the trunk and branches, then uses 
triangulation to determine 3D points. Our method algorithm uses fewer 
images, and has a comparable run time to similar plant shape reconstruction 
methods. Applications for our algorithm include robotic dormant 
pruning,\cite{tabb2017} and structural phenotyping\cite{botterill2016}.

\section{Related Work}

Ref. \cite{botterill2016} provides a more comprehensive review of existing 
work. There are two main approaches which have been used to estimate the shapes 
for leafless woody plants. Approaches typically first determine the 3D shape of 
the plant, and then determine its structure;\cite{tabb2017,dey2012,kumar2014} or 
determine the 2D structure of the plant in multiple views, and from that 
determine its 3D structure\cite{botterill2016}.

\subsection{Reconstruction-first approach}
\label{ss-other-model}

The most directly relevant method which reconstructs the 3D shape first is Ref. 
\cite{tabb2017}, which estimates tree shapes for the dormant pruning of fruit 
trees. It uses a robot to capture images of the leafless trees against a fixed 
color background, producing a tree silhouette from multiple angles. Then 
applying a shape-from-inconsistent-silhouette method\cite{tabb2014} 
simultaneously refines camera positions and calibration, and produces a 
maximal volume which fits within most of the silhouettes. The tree structure 
is then determined from the 3D skeleton of the volume. This method is very 
accurate but has a significant run time of around 8 minutes per tree.

\subsection{Vector-first approach}
\label{ss-other-vector}

An alternative approach, best exemplified by Ref. \cite{botterill2016}, 
incrementally extends a 3D model of a row of vines. Given an image, 
the underlying vine structure is determined by first identifying the edges of 
the vine segments, then connecting the edges to form boundaries for the vine 
segments, then building vine segments from the regions between the edges, and 
finally joining the vine segments together. Then a 3D model of the vine row is 
build and extended by matching the 2D structures between frames and using 
bundle adjustment to determine the exact change in camera position and the 3D 
coordinates of the cane segments.

\section{Method}
\label{s-method}

Because constructing a dataset from real-world images and measurements is 
expensive, we create a synthetic dataset instead; the details are given in 
Sec. \ref{ss-dataset}.

Our method for tree reconstruction can be divided into four stages. For each
image of a given tree, we identify the tree skeleton pixels on the image 
(Sec. \ref{ss-skel}), and estimate a likely tree structure to explain
these pixels (Sec. \ref{ss-vect}). From the resulting 2D
tree structures we determine the camera position and orientations for each
image (Sec. \ref{ss-frame}), and finally identify branches between
different images and triangulate the 3D branch shapes (Sec. 
\ref{ss-triangulation}.)

\subsection{Dataset generation}
\label{ss-dataset}

To generate a dataset of tree images and ground truth tree structure, we use 
the ”Tree Gen: Sapling” addon of the 3D modeling program 
Blender\cite{blender2018}. The addon implements Ref. \cite{weber1995} which 
provides a model for the generation of geometrically reasonable tree structures.

The addon is configured using the "Quaking Aspen" preset\cite{weber1995}, with 
some additional modifications to reduce the total number of branches and 
improve realism. Of course, leaves are disabled for this dataset. A branching 
level parameter controls the maximum (data-structure) depth of the tree, and 
is set to 2 rather than 3 to reduce the typical number of branches from about 
700 to 30. This reduction in branch count also ensures that images of the tree 
are sparse in that as crossings between branches typically involve only one pair 
at a time, and that one can almost always see the background on the sides of a 
given branch. Furthermore, because the models from Ref. \cite{weber1995} are 
designed to extend between levels of detail, branches by default taper to zero 
width at their points. As the branches of mature trees typically taper to no 
thinner than about $\unit[2]{mm}$, the default setting is unrealistic and 
makes determining the tree skeleton needlessly more difficult, as branches which 
taper to zero width fade into the background in rendered images. To avoid this 
effect, the second layer branches are restricted to taper to no more than 20\% 
their initial diameter.

\begin{figure}
\noindent \begin{centering}
\includegraphics[width=0.45\linewidth]{images/example_tree_1}\,%
\includegraphics[width=0.45\linewidth]{images/example_tree_2}
\par\end{centering}
\caption{\label{fig:treeimages}Renders of the same 3D tree as generated by the 
dataset code. Empty background regions have been cropped away.}
\end{figure}

Because the focus of this paper is on reconstructing the tree shape rather than 
identifying the tree, the rendering setup in Blender is deliberately 
simplified. The only object in the rendered scene is the tree, with the base of 
the trunk positioned at the origin. The background approximates a clear sky, as 
a vertically aligned gradient between two shades of blue. The output resolution 
is set to $W\times H=\unit[1500]{px}\times\unit[2500]{px}$ so that even the 
tips of branches are at least two pixels wide. To simplify camera frame 
reconstruction, the camera frames used for the generated images are 
parameterized by a single angle relative to the vertical axis of the scene.

\begin{figure*}[]
\small\begin{verbatim}
0 = -1: -1.15e-08 0.1316 -1.484, 0 0 0, 1.151e-08 -0.1316 1.484, 0.2288; ...
1 = 0: -0.09259 -0.6476 5.629, 4.65e-08 -0.5327 5.608, 0.09259 -0.4178 5.587, 0.02665; ...
2 = 0: 0.1577 -0.5418 5.913, 4.93e-08 -0.5645 5.875, -0.1577 -0.5871 5.836, 0.02988; ...
\end{verbatim}
\begin{verbatim}
branch_id = parent_id: left_handle, center, right_handle, radius; ...
branch_id = parent_id: left_handle, center, right_handle, radius; ...
...
\end{verbatim}
\caption{\label{fig:structure}Example tree structure. Each triple of numbers
is a point in the Bezier curve for a branch. The first and last handles only
slightly affect rendering and may be disregarded for comparison purposes.}
\end{figure*}

The generated dataset then consists of a collection of 3D tree scenes in 
Blender, and for each scene, between two and five images of the tree taken at 
random angles are provided; see Fig. \label{fig:treeimages} for an example. The 
ground truth includes the exact camera projection matrices for each images as 
well as the structure of the tree in the format given by Fig. 
\ref{fig:structure}.

\subsection{Skeleton identification}
\label{ss-skel}

The goal of this part of the algorithm is to determine an 8-connected skeleton
for a given tree image. First, we identify a foreground of pixels corresponding 
to the tree. This is done by converting the image to the CIELAB color 
space\cite{szeliski2010}, and then selecting the pixels whose Euclidean 
distance 
to a typical tree pixel
color, $\left[L,a,b\right]=\left[72.6,-4.88,19.3\right]$, is less than the 
minimum distance one of three typical sky colors, namely  
$\left[73.6,-19.7,-40.8\right]$, $\left[86.9,-41.6,-20.5\right]$, and 
$\left[81.8,-33.6,-28.2\right]$. This produces a binary mask of foreground 
pixels, to which $3\times3$ binary closing is applied to eliminate single-pixel
holes that would make junction detection in Sec. \ref{ss-vect} more 
difficult.

The foreground mask is reduced to an 8-connected skeleton in two steps. 
Following Ref. \cite{donati2018}, we first apply TPTA\cite{zhang1984} to create 
a 4-connected skeleton, and then reduce this to an 8-connected skeleton by the 
method given in Ref. \cite{donati2018}: to remove all pixels which match the 
hit-miss operator

\noindent \begin{center}
\begin{tabular}{|c|c|c|}
\hline 
 & 1 & \tabularnewline
\hline 
 & 1 & 1\tabularnewline
\hline 
0 &  & \tabularnewline
\hline 
\end{tabular}
\par\end{center}

\noindent where a 1 signifies a pixel in the skeleton, 0 a pixel outside the 
skeleton, and blank a pixel whose value is irrelevant; and then to remove all 
pixels which match the operator rotated by one quarter, then one half, and then 
three quarters of a full rotation. (This is a inelegant method of finding an 
8-connected skeleton, and methods like OPATA8\cite{deng2000} provide similar 
results but do not need to be postprocessed.)

\subsection{2D vectorization}
\label{ss-vect}

There are six steps to producing a 2D tree structure from the tree image 
skeleton. 

\begin{enumerate}

\item Identify segment endpoints and the junctions between segments. Again 
following Ref \cite{donati2018}, the hit-miss operators to identify the 20 
types of endpoints are the rotations of

\noindent 
\begin{center}
\begin{tabular}{|c|c|c|}
\hline 
 & 1 & \tabularnewline
\hline 
0 & 1 & 0\tabularnewline
\hline 
0 & 0 & 0\tabularnewline
\hline 
\end{tabular}\qquad{}%
\begin{tabular}{|c|c|c|}
\hline 
1 & 0 & 0\tabularnewline
\hline 
0 & 1 & 0\tabularnewline
\hline 
0 & 0 & 0\tabularnewline
\hline 
\end{tabular}
\par\end{center}

and the $3\!\times\!3$ hit-miss operators for the 56 possible junctions are the 
rotations of

\noindent \begin{center}
\begin{tabular}{|c|c|c|}
\hline 
 & 1 & \tabularnewline
\hline 
0 & 1 & 0\tabularnewline
\hline 
1 & 0 & 1\tabularnewline
\hline 
\end{tabular}\quad{}%
\begin{tabular}{|c|c|c|}
\hline 
1 & 0 & \tabularnewline
\hline 
0 & 1 & 0\tabularnewline
\hline 
1 & 0 & 1\tabularnewline
\hline 
\end{tabular}\quad{}%
\begin{tabular}{|c|c|c|}
\hline 
 & 0 & 1\tabularnewline
\hline 
1 & 1 & 0\tabularnewline
\hline 
 & 1 & \tabularnewline
\hline 
\end{tabular}
\par\end{center}

Because it is possible for several junction pixels to neighbor each other, 
connected sets of junction pixels are identified as a single junction whose 
associated position is the average of its components.

\item Use a depth first search to construct a graph over the skeleton, whose 
vertices are either endpoints or junctions, and whose edges are the paths in 
the skeleton between vertices. Often two curved tree branches may cross each 
other twice, leading to two distinct edges between the same pair of graph 
vertices. Loops are not recorded since they almost never occur in actual tree 
images. Segments corresponding to the link between a junction and adjacent 
endpoint are counted; these often correspond to cases where a branch tip barely 
crosses another branch. To each edge, we associate the oriented list of 
image coordinates of the skeleton pixels.

\item First, identify the root vertex; for our dataset it is easily identified 
as the vertex furthest down in the image coordinates. (Alternatively, it is the 
one whose branch is thickest, where branch thickness can be estimated by the 
image distance to the nearest background pixel.) Then for each segment in the 
graph, assign an orientation so that from any vertex in the graph one can trace 
a path to the root vertex. Segment orientations are intended to follow the 
orientations of their associated branches; an adequate proxy for this is to 
orient segments in the direction of decreasing minimum path distance to the 
root. This method ensures that the root vertex is always reachable. (In theory, 
the typical increase in branch radius toward the root could be used as an 
additional orientation cue.)

\item Construct a set of paths from endpoints to the root which most likely 
follows the actual branches of the trees. Because there may be multiple edges 
per pair of vertices, the paths are represented as sequences of edges, not 
sequences of vertices. We do this by sampling about 100 possible such path sets 
at random, selecting the one which covers the greatest total segment length. A 
given path in a path set is determined by starting at an endpoint and 
iteratively picking a random edge which extends the path and is oriented toward 
the root. To properly handle intersections between a pair of branches, a 
heuristic (see Fig. \ref{fig:heuristic}) is used whenever there are exactly 
two edges moving out from the current end of the path. As degree four junctions 
almost never result from crossings between two branches, but rather from 
multi-branch crossings, there is no heuristic for this case.

\begin{figure}
\noindent \begin{centering}
\includegraphics[width=0.9\linewidth]{images/path_heuristic}\,%
\par\end{centering}
\caption{\label{fig:heuristic}Path crossing heuristic. After continuing a path 
from the right, the next decision is to the left, and vice versa. The result is 
a path which moves straight at intersections.}
\end{figure}

\item Next, we compute a common suffix tree for the set of paths. The vertices 
of this tree are subpaths of the path set, excluding vertices which 
would, if included in the common tree, have exactly one preceding and one 
following vertex. To the edges of the tree we associate the lists of points 
on the skeleton moving from the endpoint closer to the root to the other 
endpoint.

\item Finally, we determine a branch-structured tree, whose nodes are 
associated with maximal straight paths along the pixels of the image skeleton, 
and for which the ordered children are the branches which start on junctions 
which are contained within the path associated to the node. This tree structure 
is directly comparable with the tree structure provided by the generated 
dataset in Sec. \ref{ss-dataset}. The interiors of the paths associated to a 
given tree node are also smoothed by a length-3 boxcar filter.

\end{enumerate}

\subsection{Camera frame determination}
\label{ss-frame}

In order to keep our solution simple and reasonably fast, we include only one 
degree of freedom in the position and orientation of the cameras in the 
generated dataset. Although our method likely can generalize to determine the 
full 6D combination of position and orientation, doing so 
efficiently 
is difficult. Fortunately, given the method to determine the relative camera 
frame between two images $i$ and $j$ -- which for our dataset, corresponds to a 
single angle $\theta_{i,j}$ in $\left[0,2\pi\right)$ -- the relative 
orientations $\alpha_k$ of all $n\in\left\{2,3,4,5\right\}$ images can be 
found by minimizing over $\alpha_k$ the value
\[
\sum_{i=1}^{n}\sum_{j=1}^{i-1}d_{S^{1}}\left(\left(\alpha_{k}-\alpha_{j}
\right)-\theta_{k,j}\right)^{2}
\]
where the circle distance function is
\[
d_{S^{1}}\left(a,b\right)=\min\left(\left|a-b\right|,2-\left|a-b\right|\right)
\]

So, to determine the relative camera frame between a pair of images, we again 
minimize a cost function over the orientation difference. Using the method from 
Sec. \ref{sss-triangulate}, we can for each branch endpoint 2D 
coordinate $x_i$ and $y_j$, compute a 3D position $Z_{i,j}$ and project it onto 
$\hat{x}_i$ and $\hat{y}_j$ in the original images. Then we define $\sigma_i$ 
as the distance to the closest other endpoint to $x_i$ in image coordinates, 
and $\rho_{x,i}=\min\left(\sigma_i,\left\Vert x_i, 
\hat{x}_i\right\Vert\right)$. Then the function to minimize is
\[
\sum_{i}\min_{j}\left\{ \rho_{x,i}^{2}+\rho_{y,i}^{2}\right\} 
+\sum_{j}\min_{i}\left\{ \rho_{x,i}^{2}+\rho_{y,i}^{2}\right\} 
\]

\subsubsection{Reconstructing a 3D point given its projections}
\label{sss-triangulate}

Given two images created by the $3\times4$ projection matrices $P$ and $P'$ 
corresponding to cameras centered at $C$ and $C'$, our goal is to find a point 
$\hat{X}$ in 3D space which projects onto points $x$ and $x'$ in 
the first and second images. The set of points which project onto $x$ is a line 
through $x$ and $C$ similarly for $x'$ and $C'$, as the following image from 
Ref. \cite{Hartley2017} shows:
\noindent \begin{center}
\includegraphics[width=0.65\columnwidth]{images/SkewLines.PNG}
\end{center}
Moving to projective geometry, all points $X$ on the line through $x$ and $C$ 
solve the equation $x\times PX=0$; and the points $X$ on the line through $x'$ 
and $C'$ fulfill $x'\times PX'=0$. A point $X$ in projective space closest to 
both skew lines can be determined by solving $AX=0$ for nonzero $X$, where 
\[
A=\begin{bmatrix}x\times P\\
x'\times P'
\end{bmatrix}
\]
the cross products by $x$ and $x'$ are applied to each column of $P$ and $P'$. 
Then dividing $X$ by its fourth (scale) coordinate produces $\hat{X}$ 
a point in 3D coordinate space.\cite{Hartley2017}

Our implementation solves the equation $AX=0$ by computing the singular value 
decomposition of the matrix $A$, and then selecting the eigenvector whose 
associated eigenvalue has the smallest absolute value. 

\subsection{3D tree reconstruction}
\label{ss-triangulation}

The final stage of our method is to construct a 3D tree model given the 2D 
tree models and the camera frames determined in the previous stage. Using the 
reconstruction method in Sec. \ref{sss-triangulate}, we identify branches in 
the 2D tree models for which the branch endpoints can be identified in 3D space 
and when projected back onto each image, fall close to the 2D branch endpoints. 
Although projection distorts branch lengths in each view, branches are 
typically short enough and far enough away from that camera that e.g. the point 
halfway on the 3D branch projects almost exactly onto the halfway point 
of each of the branch's projections. Finally, the collection of 3D branches
structure is stitched together into a tree structure, where the parent of a 
3D branch is the 3D branch corresponding to the most 2D branches which are 
ancestors of the 2D branches corresponding to the original branch.

\section{Experiments}


\begin{figure}
\noindent \begin{centering}
\includegraphics[width=0.45\linewidth]{images/output_good_0_0}\,%
\includegraphics[width=0.45\linewidth]{images/output_bad_3_0}
\par\end{centering}
\caption{\label{fig:output}Black lines are the reconstructed image. Left: A 
good image reconstruction from 2 frames, with 10 branches, $3.34\times10^{-6}$ 
frame error, and $0.084$ position error. Right: A bad image reconstruction from 
5 frames, with 27 branches, $0.109$ frame error, and $0.860$ position error.}
\end{figure}

We investigate the runtime of our method as well as the quality of results its 
stability to image degradation. Tests are all performed single threaded on 
an otherwise idle server with a Xeon E5-2420 CPU\cite{cpu_cycle1}, using the 
Python implementation of our algorithm. An implementation in Matlab runs 
comparably quickly, but has as yet unresolved bugs leading to slightly 
reduced output quality. A generated dataset with 100 scenes in total is used.
Fig. \ref{fig:output} shows an example of a good reconstruction, as well 
as an example of a bad reconstruction.

\subsection{Runtime}

\begin{figure}
\noindent \begin{centering}
\includegraphics[width=0.9\linewidth]{images/plot-times}\,%
\par\end{centering}
\caption{\label{fig:runtime}Runtimes for the different stages of our tree 
reconstruction method as a function of the number of input images.}
\end{figure}

The recorded average runtime as a function of the number of images is given in 
Fig. \ref{fig:runtime}. Skeletonization and vectorization, Secs. 
\ref{ss-skel} and \ref{ss-vect}, have linear runtime in the number of images.
As relative camera frames are determined for each pair of images, runtime is 
quadratic. Finally, the time needed to reconstruct the tree is dominated by the 
time to find coordinates for intermediate points on branches, which grows 
linearly in the number of reconstructed branches.

\subsection{Result quality}

\subsubsection{Number of branches}

\begin{figure}
\noindent \begin{centering}
\includegraphics[width=0.9\linewidth]{images/plot-nb}\,%
\par\end{centering}
\caption{\label{fig:nbranches}Mean number of branches recovered (and standard 
deviation of the mean) for a given number of input images.}
\end{figure}

The number of branches in the reconstructed tree should match the ground truth.
In practice, as Fig. \ref{fig:nbranches} shows, with more frames it is more 
likely that the 2D branches are matched in Sec. \ref{ss-triangulation}.

\subsubsection{Frame reconstruction accuracy}

As the camera calibration matrix $K$ is known, we extract the rotation 
matrix $R$ of the camera in a given frame from the projection matrix 
$P=K\left[R|t\right]$. Then the rotation matrices $\hat{R}_i$ determined in 
Sec. \ref{ss-frame} can be compared with the ground truth camera rotation 
matrices $R_i$. The angle between the two rotations is
\[
\theta_{i}=\arccos\left(\frac{1}{2}\left(\mathrm{tr}\left(R_{i}^{-1}\hat{R}_{i}
\right)-1\right)\right)
\]
and a total score is provided by
\[
\frac{1}{n}\sum_{i=1}^{n}\theta_{i}^{2}
\]
which is $1$ if the reconstructed orientation matrices are exactly opposite the 
actual orientation matrices, and approaches $0$ as accuracy improves. Because 
the absolute orientations can not be determined, we reorient both sets of 
rotation matrices matrices so that $R_i$ and $\hat{R}_i$ are the identity; then 
the worst case score is still $\frac{n-1}{n}$.

\begin{figure}
\noindent \begin{centering}
\includegraphics[width=0.9\linewidth]{images/plot-af}\,%
\par\end{centering}
\caption{\label{fig:frameerr}Mean and standard deviation of mean for the 
frame orientation error.}
\end{figure}

As Fig. \ref{fig:frameerr} shows, the orientation reconstruction accuracy 
\emph{decreases} as the number of images increases. With more images, more 
relative camera frames must be evaluated, and a single badly reconstructed 
frame can introduce significant error when finding a common set of camera 
frames.

\subsubsection{Branch position accuracy}

To measure the accuracy with which branches are placed, we consider the ground 
truth and reconstructed trees as point sets, including both branch endpoints 
and intermediate points. Then with $x_i$ the $n_x$ points of the ground truth 
tree, and $y_j$ the $n_y$ points of the reconstructed tree, define
\[
M=\frac{1}{n_{x}}\sum_{i}\min_{j}\left\Vert x_{i}-y_{j}\right\Vert 
+\frac{1}{n_{y}}\sum_{j}\min_{i}\left\Vert x_{i}-y_{j}\right\Vert 
\]
\[
S=\sqrt{\left(\frac{1}{n_{x}}\sum_{i}\left\Vert x_{i}\right\Vert 
\right)\left(\frac{1}{n_{y}}\sum_{j}\left\Vert y_{j}\right\Vert \right)}
\]
(with $\left\Vert\cdot\right\Vert$ Euclidean distance) so that $M/S$ gives an 
scale-invariant accuracy score which reaches $0$ for identical trees. $S$ is 
not 
position invariant, but this does not matter as the root of the tree is fixed 
at the origin; rotation invariance is ensured by rotating all the reconstructed 
frames so that the first reconstructed and true camera frames match.

\begin{figure}
\noindent \begin{centering}
\includegraphics[width=0.9\linewidth]{images/plot-ap}\,%
\par\end{centering}
\caption{\label{fig:poserr}Mean and standard deviation of mean for the 
reconstructed tree position error.}
\end{figure}

As expected, Fig. \ref{fig:poserr} shows improved accuracy is the number of 
images increases. This may, however, be an artifact of the increased number
of branches for larger image counts, as these additional branches may better
fill the volume spanned by the original tree.

\subsection{Degradation tests}

\subsubsection{Image resolution}

We vary the input image resolution by downscaling the input images from 
$\unit[1500]{px}\times\unit[2500]{px}$ to $\unit[750]{px}\times\unit[1250]{px}$
and $\unit[375]{px}\times\unit[615]{px}$. This change reduces runtime for all 
stages but frame determination, but doubles average position and frame errors 
for the smallest image size, while reducing the number of identified branches 
by 20\%.

\subsubsection{Image noise}

Considering the input images as $1500\times2500\times3$ arrays of values in 
$\left[0,1\right]$, we introduce gaussian noise on every array element with 
magnitudes $0.05$, $0.15$, and $0.25$, and clip the results to 
$\left[0,1\right]$. The number of branches increases by less than 10\% up to 
the $0.15$ noise level, but at $0.25$, foreground detection and skeletonization 
break down, producing five times more branches than expected. At 
$0.25$, position error doubles, now getting worse as the number of images 
increases, and frame error triples. Finally, at $0.25$, the increased number of 
branches increases runtime (to up to an hour) as the camera frame evaluation 
runs in time quadratic in the number of branches, and 40\% of fit attempts fail.

\section{Conclusion and discussion}

We have presented a method for the reconstruction of the 3D form of a leafless 
tree using multiple images taken at different angles. We evaluate our method on 
a synthetic dataset, and find that even though camera frame reconstruction 
quality decreases slightly as the number of images increases from two to five, 
the average position error of the branches is reduced, possibly because the
increased number of images permits the identification of more branches. 

Although the implementation of our method runs slowly due to several 
$O\left(n^2\right)$ operations in the camera frame reconstruction phase, 
it has the potential for factor 20 speedups with straightforward optimization, 
and the use of standard bundle adjustment techniques\cite{forsyth2011} may 
reduce the algorithmic complexity.

In retrospect, determining the 2D tree structure by starting with 
skeletonization may not be as effective as the method of Ref. 
\cite{botterill2016}, who use the boundaries rather than the centerlines of 
branches to determine their shapes, because skeletonization effectively removes 
information about crossings which must later be reconstructed with heuristics.

Future work could experiment with more complicated tree structures, multiple 
leafless trees, or handle real images of trees, measuring the process accuracy 
by the accuracy of the reprojected 3D tree models.

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
